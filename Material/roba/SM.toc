\select@language {english}
\contentsline {chapter}{\numberline {1}Physics of flavor}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}The SM Lagrangian}{7}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Bosons}{8}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Fermions and flavor}{10}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Discrete symmetries }{13}{subsection.1.1.3}
\contentsline {subsubsection}{Sakharov conditions}{17}{subsubsection*.6}
\contentsline {section}{\numberline {1.2}Neutrino masses}{18}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Dirac and Majorana mass terms}{18}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}The SM flavor group}{21}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Processes}{22}{subsection.1.2.3}
\contentsline {subsubsection}{Muon decay}{22}{subsubsection*.7}
\contentsline {subsubsection}{Lepton flavor violation}{23}{subsubsection*.8}
\contentsline {subsubsection}{Neutrinoless double beta decay}{24}{subsubsection*.9}
\contentsline {section}{\numberline {1.3}Chiral symmetries of QCD }{25}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}$V_{\mathrm {CKM}}$ matrix elements}{28}{subsection.1.3.1}
\contentsline {subsubsection}{$|V_{ud}|$ -- superallowed $\beta -$decays}{28}{subsubsection*.13}
\contentsline {subsubsection}{$|V_{us}|$ -- semileptonic kaon decay}{30}{subsubsection*.14}
\contentsline {subsubsection}{$|V_{cb}|$ and $|V_{ub}|$ -- $B$ mesons semileptonic decays}{31}{subsubsection*.15}
\contentsline {subsubsection}{Summary and $|V_{ti}|$ elements}{32}{subsubsection*.16}
\contentsline {section}{\numberline {1.4}Unitarity triangle and $CP$ violation}{32}{section.1.4}
\contentsline {section}{\numberline {1.5}Lepton flavor universality}{37}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Pion decay}{37}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Tau decays}{40}{subsection.1.5.2}
\contentsline {section}{\numberline {1.6}Neutrino oscillation}{42}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Oscillation in vacuum}{42}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Oscillation in matter and the MSW effect}{47}{subsection.1.6.2}
\contentsline {chapter}{\numberline {2}Electroweak gauge theory }{55}{chapter.2}
\contentsline {section}{\numberline {2.1}Construction of EFTs}{55}{section.2.1}
\contentsline {subsubsection}{$\psi -\phi $ toy model}{55}{subsubsection*.22}
\contentsline {subsubsection}{Fermi theory and the values of $G_F$ and $v$}{57}{subsubsection*.23}
\contentsline {subsubsection}{See-saw mechanism and the dim 5 Weinberg operator}{59}{subsubsection*.24}
\contentsline {section}{\numberline {2.2}NC processes and the values of $\lowercase {g}_V$ and $\lowercase {g}_A$ }{59}{section.2.2}
\contentsline {section}{\numberline {2.3}The \lowercase {$e^+ e^- \rightarrow \mu ^+\mu ^-$} process in the SM}{64}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Low energy limit $G_Fs\ll 1$}{64}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}LEP energy limit: $\sqrt {s}\sim M_Z$}{67}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}EFT and unitarity bounds}{72}{section.2.4}
\contentsline {section}{\numberline {2.5}The test of the non abelian structure of the SM}{74}{section.2.5}
\contentsline {section}{\numberline {2.6}The Goldstone boson equivalence theorem}{79}{section.2.6}
\contentsline {chapter}{\numberline {3}Phenomenology at loop level}{87}{chapter.3}
\contentsline {section}{\numberline {3.1}Renormalization and dimensional regularization}{88}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Renormalization program: two steps}{89}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Dimensional regularization and subtraction scheme}{89}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Vacuum polarization}{94}{section.3.2}
\contentsline {section}{\numberline {3.3}Electron self-energy}{98}{section.3.3}
\contentsline {section}{\numberline {3.4}Vertex correction}{100}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}The anomalous magnetic moment $g-2$}{102}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}The vertex renormalization and Ward identity of QED}{104}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}ElectroWeak Precision Tests}{106}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Oblique corrections}{108}{subsection.3.5.1}
\contentsline {subsubsection}{Computation of EW vacuum polarization loops}{114}{subsubsection*.31}
\contentsline {subsection}{\numberline {3.5.2}Custodial $SU(2)_C$ symmetry}{116}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Flavor Changing Neutral Currents}{121}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Meson-antimeson oscillation}{121}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}FCNC in the SM and the GIM mechanism}{125}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}The hierarchy problem}{127}{subsection.3.6.3}
\contentsline {section}{\numberline {3.7}Anomalies}{128}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Axial-vector current Ward identity anomaly and $\pi ^0 \rightarrow \gamma \gamma $}{129}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}ABJ anomalies in non-abelian theories }{135}{subsection.3.7.2}
\contentsline {section}{\numberline {3.8}Higgs precision physics}{137}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Theoretical bounds on $M_h$: triviality and vacuum stability bounds}{137}{subsection.3.8.1}
\contentsline {subsection}{\numberline {3.8.2}Experimental bounds on $M_h$}{141}{subsection.3.8.2}
\contentsline {subsection}{\numberline {3.8.3}Higgs production: gluon-gluon fusion and the parton model}{144}{subsection.3.8.3}
\contentsline {subsection}{\numberline {3.8.4}Higgs decay modes}{145}{subsection.3.8.4}
\contentsline {chapter}{\numberline {4}Exercises}{151}{chapter.4}
\contentsline {section}{\numberline {4.1}Assignement 1}{151}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Exercise 1}{151}{subsection.4.1.1}
\contentsline {subparagraph}{Text}{151}{subparagraph*.42}
\contentsline {subparagraph}{Solution}{151}{subparagraph*.43}
\contentsline {subsection}{\numberline {4.1.2}Exercise 3}{155}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Exercise 4}{156}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Exercise 5}{160}{subsection.4.1.4}
\contentsline {subparagraph}{Text}{160}{subparagraph*.46}
\contentsline {subparagraph}{Solution}{160}{subparagraph*.47}
\contentsline {chapter}{\numberline {A}Reference formulae}{167}{appendix.A}
\contentsline {section}{\numberline {A.1}$\lowercase {\gamma }$ matrices and Fierz identities}{167}{section.A.1}
\contentsline {section}{\numberline {A.2}Cross sections and decay rates}{168}{section.A.2}
