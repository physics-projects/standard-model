set term epslatex
set samples 1000000
set logscale x
set out "osc.tex"
set contour
f(x)=1-sin(2*1)**2*sin(1.27*x)**2
g(x)=1-sin(2*1)**2
h(x)= 1-0.5*sin(2*1)**2
set ylabel "Probability $\\nu_e \\rightarrow \\nu_e $"
set xlabel "Neutrino Flight Distance $L$ [km] "
set zlabel "$\\frac{1}{2}\\sin 2 \\beta$"
plot [0.1:10**3] [0:1.3] f(x) t "$\\mathcal{P}_{ee}=1-\\sin^2 2 \\theta \\sin^2 \\left[1.27L\\right]$" lc rgb "black" ,g(x) lc rgb "black" dt 2 t "$1-\\sin^2 2 \\theta$", h(x) lc rgb "black" dt 4 t "$1-\\frac{1}{2}\\sin^2 2 \\theta$" 
set output

