set term epslatex size 3,3
set samples 1000
set out "copuling.tex"
set ylabel "$\\alpha^{-1}$ inverse coupling  strength"
set xlabel "Energy $\\log\\left(\\frac{k}{\\mathrm{GeV}}\\right)$ "
plot "coupling_em.txt" t "$1/\\alpha_{em}(k)$"  w lines lc rgb "red" lw 3, "coupling_W.txt"  w lines lc rgb "blue" dt 2 lw 3 t "$1/\\alpha_{W}(k)$", "coupling_s.txt"  w lines lc rgb "black" dt 4 lw 3 t "$1/\\alpha_{s}(k)$" 
set output

