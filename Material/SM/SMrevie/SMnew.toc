\select@language {english}
\contentsline {chapter}{\numberline {1}The Standard Model Lagrangian}{11}{chapter.1}
\contentsline {section}{\numberline {1.1}The bosonic sector}{12}{section.1.1}
\contentsline {section}{\numberline {1.2}The fermionic sector}{14}{section.1.2}
\contentsline {section}{\numberline {1.3}The CKM matrix}{16}{section.1.3}
\contentsline {chapter}{\numberline {2}Discrete symmetries }{19}{chapter.2}
\contentsline {section}{\numberline {2.1}$C$, $P$ and $T$ symmetries}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}$C$, $P$ and $T$ in the Standard Model}{20}{section.2.2}
\contentsline {section}{\numberline {2.3}Sakharov conditions}{22}{section.2.3}
\contentsline {chapter}{\numberline {3}Neutrino physics}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Neutrino masses}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}The see-saw mechanism}{26}{section.3.2}
\contentsline {section}{\numberline {3.3}The PMNS matrix}{27}{section.3.3}
\contentsline {section}{\numberline {3.4}The Standard Model flavor group}{28}{section.3.4}
\contentsline {section}{\numberline {3.5}Phenomenology of massive neutrinos}{30}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Lepton flavor violation}{30}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Neutrinoless double beta decay}{31}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Neutrino oscillation}{32}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Oscillation in the vacuum}{33}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Oscillation in the matter and the MSW effect}{38}{subsection.3.6.2}
\contentsline {chapter}{\numberline {4}Determination of the CKM matrix elements}{47}{chapter.4}
\contentsline {section}{\numberline {4.1}Chiral symmetries of QCD }{47}{section.4.1}
\contentsline {section}{\numberline {4.2}$|V_{ud}|$ from superallowed $\beta $-decays}{50}{section.4.2}
\contentsline {section}{\numberline {4.3}$|V_{us}|$ from semileptonic kaon decays}{52}{section.4.3}
\contentsline {section}{\numberline {4.4}$|V_{cb}|$ and $|V_{ub}|$ from semileptonic $B$ meson decays}{53}{section.4.4}
\contentsline {section}{\numberline {4.5}$|V_{td}|$ and $|V_{ts}|$ from $B$-$\mkern 1.5mu\overline {\mkern -1.5muB\mkern -1.5mu}\mkern 1.5mu$ oscillation}{54}{section.4.5}
\contentsline {section}{\numberline {4.6}Unitarity triangle and $CP$ violation}{54}{section.4.6}
\contentsline {chapter}{\numberline {5}Lepton flavor universality }{59}{chapter.5}
\contentsline {section}{\numberline {5.1}Pion decay $\pi \DOTSB \relbar \joinrel \rightarrow \ell \nu _\ell $}{59}{section.5.1}
\contentsline {section}{\numberline {5.2}Radiative corrections for $\pi \DOTSB \relbar \joinrel \rightarrow \ell \nu _\ell $ }{61}{section.5.2}
\contentsline {chapter}{\numberline {6}Effective Field Theories}{65}{chapter.6}
\contentsline {section}{\numberline {6.1}General construction}{65}{section.6.1}
\contentsline {section}{\numberline {6.2}$\psi -\phi $ Yukawa toy model}{65}{section.6.2}
\contentsline {section}{\numberline {6.3}Fermi theory}{67}{section.6.3}
\contentsline {section}{\numberline {6.4}The Weinberg operator}{69}{section.6.4}
\contentsline {section}{\numberline {6.5}Partial wave unitarity bounds}{69}{section.6.5}
\contentsline {chapter}{\numberline {7}The Standard Model before LEP and the weak NC discovery}{73}{chapter.7}
\contentsline {section}{\numberline {7.1}\lowercase {$\nu _\mu e^- \rightarrow \nu _\mu e^- $} and \lowercase {$\mkern 1.5mu\overline {\mkern -1.5mu\nu \mkern -1.5mu}\mkern 1.5mu_\mu e^- \rightarrow \mkern 1.5mu\overline {\mkern -1.5mu\nu \mkern -1.5mu}\mkern 1.5mu_\mu e^- $}}{73}{section.7.1}
\contentsline {section}{\numberline {7.2} \lowercase {$ {\nu }_e e^- \rightarrow {\nu }_e e^-$} and \lowercase {$\mkern 1.5mu\overline {\mkern -1.5mu\nu \mkern -1.5mu}\mkern 1.5mu_e e^- \rightarrow \mkern 1.5mu\overline {\mkern -1.5mu\nu \mkern -1.5mu}\mkern 1.5mu_e e^-$}}{76}{section.7.2}
\contentsline {section}{\numberline {7.3} \lowercase {$e^+ e^- \rightarrow \mu ^+\mu ^-$} cross section and forward-backeard asymmetry }{77}{section.7.3}
\contentsline {chapter}{\numberline {8}The Standard Model at LEP}{81}{chapter.8}
\contentsline {section}{\numberline {8.1}The optical theorem and the $Z$ boson propagator}{81}{section.8.1}
\contentsline {section}{\numberline {8.2}\lowercase {$e^+e^- \rightarrow \mu ^+\mu ^-$} cross-section and forward-backward asymmetry}{83}{section.8.2}
\contentsline {section}{\numberline {8.3}The determination of $M_Z$, $\Gamma _Z$ and the neutrino number $N_\nu $}{84}{section.8.3}
\contentsline {section}{\numberline {8.4}The non abelian structure of the Standard Model: \lowercase {$e^+e^- \rightarrow $}$ W^+W^- $}{85}{section.8.4}
\contentsline {subsubsection}{Amplitude for $e_R^-$}{88}{subsubsection*.18}
\contentsline {section}{\numberline {8.5}The Goldstone boson equivalence theorem}{90}{section.8.5}
\contentsline {subsection}{\numberline {8.5.1}${e^+e^- \rightarrow W^+ W^-}$}{92}{subsection.8.5.1}
\contentsline {subsection}{\numberline {8.5.2}${t\rightarrow W^+ b}$}{93}{subsection.8.5.2}
\contentsline {subsection}{\numberline {8.5.3}$W^+W^-\rightarrow W^+ W^-$ and the no-lose-Higgs theorem}{95}{subsection.8.5.3}
\contentsline {chapter}{\numberline {9}Renormalization}{99}{chapter.9}
\contentsline {section}{\numberline {9.1}Dimensional regularization and subtraction schemes}{100}{section.9.1}
\contentsline {section}{\numberline {9.2}Vacuum polarization}{106}{section.9.2}
\contentsline {section}{\numberline {9.3}Electron self-energy}{111}{section.9.3}
\contentsline {section}{\numberline {9.4}Vertex correction}{112}{section.9.4}
\contentsline {section}{\numberline {9.5}The anomalous magnetic moment $g-2$}{114}{section.9.5}
\contentsline {section}{\numberline {9.6}Ward identity of QED}{117}{section.9.6}
\contentsline {chapter}{\numberline {10}Electro-Weak Precision Tests}{123}{chapter.10}
\contentsline {section}{\numberline {10.1}Oblique corrections}{125}{section.10.1}
\contentsline {section}{\numberline {10.2}Computation of EW vacuum polarization loops}{130}{section.10.2}
\contentsline {section}{\numberline {10.3}Custodial $SU(2)_C$ symmetry}{133}{section.10.3}
\contentsline {section}{\numberline {10.4}Corrections to the $\rho $ - parameter}{136}{section.10.4}
\contentsline {subsection}{\numberline {10.4.1}Top-quark contribution }{136}{subsection.10.4.1}
\contentsline {subsection}{\numberline {10.4.2}Higgs-boson contribution}{137}{subsection.10.4.2}
\contentsline {section}{\numberline {10.5}Corrections to the $Z$ boson partial decay widths*}{140}{section.10.5}
\contentsline {section}{\numberline {10.6}Corrections in the \textit {gaugeless limit}* }{144}{section.10.6}
\contentsline {subsection}{\numberline {10.6.1}Corrections to the $\rho $ - parameter}{146}{subsection.10.6.1}
\contentsline {subsection}{\numberline {10.6.2}Correction to the $Z \rightarrow \mkern 1.5mu\overline {\mkern -1.5mub\mkern -1.5mu}\mkern 1.5mub$ decay rate}{149}{subsection.10.6.2}
\contentsline {section}{\numberline {10.7}The $\mathcal {S}$, $\mathcal {T}$ and $\mathcal {U}$ parameters*}{151}{section.10.7}
\contentsline {chapter}{\numberline {11}Flavor Changing Neutral Currents}{153}{chapter.11}
\contentsline {section}{\numberline {11.1}Meson-antimeson oscillation}{153}{section.11.1}
\contentsline {section}{\numberline {11.2}FCNC in the SM and the GIM mechanism}{157}{section.11.2}
\contentsline {chapter}{\numberline {12}Anomalies}{161}{chapter.12}
\contentsline {section}{\numberline {12.1}Axial-vector current anomaly and $\pi ^0 \rightarrow \gamma \gamma $}{162}{section.12.1}
\contentsline {section}{\numberline {12.2}ABJ anomalies}{163}{section.12.2}
\contentsline {section}{\numberline {12.3}Anomalies in the Standard Model }{168}{section.12.3}
\contentsline {chapter}{\numberline {13}Higgs physics}{171}{chapter.13}
\contentsline {section}{\numberline {13.1}Theoretical bounds on $M_h$: triviality and vacuum stability bounds}{171}{section.13.1}
\contentsline {section}{\numberline {13.2}Experimental bounds on $M_h$}{186}{section.13.2}
\contentsline {section}{\numberline {13.3}Higgs production: gluon-gluon fusion and the parton model}{188}{section.13.3}
\contentsline {section}{\numberline {13.4}Higgs decay modes}{190}{section.13.4}
\contentsline {subsection}{\numberline {13.4.1}\lowercase {$h\rightarrow \mkern 1.5mu\overline {\mkern -1.5muf\mkern -1.5mu}\mkern 1.5muf$}}{191}{subsection.13.4.1}
\contentsline {subsection}{\numberline {13.4.2}\lowercase {$h\rightarrow gg$ and $h\rightarrow \gamma \gamma $}}{192}{subsection.13.4.2}
\contentsline {subsubsection}{Full calculation of the diagrams*}{193}{subsubsection*.33}
\contentsline {chapter}{\numberline {14}Exercises}{205}{chapter.14}
\contentsline {section}{\numberline {14.1}Exercise 1}{205}{section.14.1}
\contentsline {subparagraph}{Text}{205}{subparagraph*.34}
\contentsline {subparagraph}{Solution}{205}{subparagraph*.35}
\contentsline {section}{\numberline {14.2}Exercise 2}{209}{section.14.2}
\contentsline {section}{\numberline {14.3}Exercise 3}{218}{section.14.3}
\contentsline {section}{\numberline {14.4}Exercise 4}{222}{section.14.4}
\contentsline {section}{\numberline {14.5}Exercise 5}{225}{section.14.5}
\contentsline {subparagraph}{Text}{225}{subparagraph*.42}
\contentsline {subparagraph}{Solution}{226}{subparagraph*.43}
\contentsline {chapter}{\numberline {A}Reference formulae}{233}{appendix.A}
\contentsline {section}{\numberline {A.1}$\lowercase {\gamma }$ matrices and Fierz identities}{233}{section.A.1}
\contentsline {section}{\numberline {A.2}Cross sections and decay rates}{234}{section.A.2}
