set term epslatex
set isosamples 50
set out "plot.tex"
set palette defined (-1 "blue", 0 "white", 1 "red")
set pm3d
z(x,y)=(1-x)*y/((1-x)**2+y**2)
set contour
set ylabel "$\\eta$"
set xlabel "$\\rho$"
set zlabel "$\\frac{1}{2}\\sin 2 \\beta$"
splot [-4:5] [-4:4] z(x,y) t "$S_{\\psi k_{s}}(\\rho,\\eta)$" lc rgb "black" 
set output

