\BOOKMARK [0][-]{chapter.1}{The Standard Model Lagrangian}{}% 1
\BOOKMARK [1][-]{section.1.1}{The bosonic sector}{chapter.1}% 2
\BOOKMARK [1][-]{section.1.2}{The fermionic sector}{chapter.1}% 3
\BOOKMARK [1][-]{section.1.3}{The CKM matrix}{chapter.1}% 4
\BOOKMARK [0][-]{chapter.2}{Discrete symmetries }{}% 5
\BOOKMARK [1][-]{section.2.1}{C, P and T symmetries}{chapter.2}% 6
\BOOKMARK [1][-]{section.2.2}{C, P and T in the Standard Model}{chapter.2}% 7
\BOOKMARK [1][-]{section.2.3}{Sakharov conditions}{chapter.2}% 8
\BOOKMARK [0][-]{chapter.3}{Neutrino physics}{}% 9
\BOOKMARK [1][-]{section.3.1}{Neutrino masses}{chapter.3}% 10
\BOOKMARK [1][-]{section.3.2}{The see-saw mechanism}{chapter.3}% 11
\BOOKMARK [1][-]{section.3.3}{The PMNS matrix}{chapter.3}% 12
\BOOKMARK [1][-]{section.3.4}{The Standard Model flavor group}{chapter.3}% 13
\BOOKMARK [1][-]{section.3.5}{Phenomenology of massive neutrinos}{chapter.3}% 14
\BOOKMARK [2][-]{subsection.3.5.1}{Lepton flavor violation}{section.3.5}% 15
\BOOKMARK [2][-]{subsection.3.5.2}{Neutrinoless double beta decay}{section.3.5}% 16
\BOOKMARK [1][-]{section.3.6}{Neutrino oscillation}{chapter.3}% 17
\BOOKMARK [2][-]{subsection.3.6.1}{Oscillation in the vacuum}{section.3.6}% 18
\BOOKMARK [2][-]{subsection.3.6.2}{Oscillation in the matter and the MSW effect}{section.3.6}% 19
\BOOKMARK [0][-]{chapter.4}{Determination of the CKM matrix elements}{}% 20
\BOOKMARK [1][-]{section.4.1}{Chiral symmetries of QCD }{chapter.4}% 21
\BOOKMARK [1][-]{section.4.2}{|Vud| from superallowed -decays}{chapter.4}% 22
\BOOKMARK [1][-]{section.4.3}{|Vus| from semileptonic kaon decays}{chapter.4}% 23
\BOOKMARK [1][-]{section.4.4}{|Vcb| and |Vub| from semileptonic B meson decays}{chapter.4}% 24
\BOOKMARK [1][-]{section.4.5}{|Vtd| and |Vts| from B-1.5mu-1.5muB-1.5mu1.5mu oscillation}{chapter.4}% 25
\BOOKMARK [1][-]{section.4.6}{Unitarity triangle and CP violation}{chapter.4}% 26
\BOOKMARK [0][-]{chapter.5}{Lepton flavor universality }{}% 27
\BOOKMARK [1][-]{section.5.1}{Pion decay -3mu}{chapter.5}% 28
\BOOKMARK [1][-]{section.5.2}{Radiative corrections for -3mu }{chapter.5}% 29
\BOOKMARK [0][-]{chapter.6}{Effective Field Theories}{}% 30
\BOOKMARK [1][-]{section.6.1}{General construction}{chapter.6}% 31
\BOOKMARK [1][-]{section.6.2}{- Yukawa toy model}{chapter.6}% 32
\BOOKMARK [1][-]{section.6.3}{Fermi theory}{chapter.6}% 33
\BOOKMARK [1][-]{section.6.4}{The Weinberg operator}{chapter.6}% 34
\BOOKMARK [1][-]{section.6.5}{Partial wave unitarity bounds}{chapter.6}% 35
\BOOKMARK [0][-]{chapter.7}{The Standard Model before LEP and the weak NC discovery}{}% 36
\BOOKMARK [1][-]{section.7.1}{e- e- \040and 1.5mu-1.5mu-1.5mu1.5mue- 1.5mu-1.5mu-1.5mu1.5mue- }{chapter.7}% 37
\BOOKMARK [1][-]{section.7.2}{ \040e e- e e- and 1.5mu-1.5mu-1.5mu1.5mue e- 1.5mu-1.5mu-1.5mu1.5mue e-}{chapter.7}% 38
\BOOKMARK [1][-]{section.7.3}{ e+ e- +- cross section and forward-backeard asymmetry }{chapter.7}% 39
\BOOKMARK [0][-]{chapter.8}{The Standard Model at LEP}{}% 40
\BOOKMARK [1][-]{section.8.1}{The optical theorem and the Z boson propagator}{chapter.8}% 41
\BOOKMARK [1][-]{section.8.2}{e+e- +- cross-section and forward-backward asymmetry}{chapter.8}% 42
\BOOKMARK [1][-]{section.8.3}{The determination of MZ, Z and the neutrino number N}{chapter.8}% 43
\BOOKMARK [1][-]{section.8.4}{The non abelian structure of the Standard Model: e+e- \040W+W- }{chapter.8}% 44
\BOOKMARK [1][-]{section.8.5}{The Goldstone boson equivalence theorem}{chapter.8}% 45
\BOOKMARK [2][-]{subsection.8.5.1}{e+e- W+ W-}{section.8.5}% 46
\BOOKMARK [2][-]{subsection.8.5.2}{tW+ b}{section.8.5}% 47
\BOOKMARK [2][-]{subsection.8.5.3}{W+W-W+ W- and the no-lose-Higgs theorem}{section.8.5}% 48
\BOOKMARK [0][-]{chapter.9}{Renormalization}{}% 49
\BOOKMARK [1][-]{section.9.1}{Dimensional regularization and subtraction schemes}{chapter.9}% 50
\BOOKMARK [1][-]{section.9.2}{Vacuum polarization}{chapter.9}% 51
\BOOKMARK [1][-]{section.9.3}{Electron self-energy}{chapter.9}% 52
\BOOKMARK [1][-]{section.9.4}{Vertex correction}{chapter.9}% 53
\BOOKMARK [1][-]{section.9.5}{The anomalous magnetic moment g-2}{chapter.9}% 54
\BOOKMARK [1][-]{section.9.6}{Ward identity of QED}{chapter.9}% 55
\BOOKMARK [0][-]{chapter.10}{Electro-Weak Precision Tests}{}% 56
\BOOKMARK [1][-]{section.10.1}{Oblique corrections}{chapter.10}% 57
\BOOKMARK [1][-]{section.10.2}{Computation of EW vacuum polarization loops}{chapter.10}% 58
\BOOKMARK [1][-]{section.10.3}{Custodial SU\(2\)C symmetry}{chapter.10}% 59
\BOOKMARK [1][-]{section.10.4}{Corrections to the \040- parameter}{chapter.10}% 60
\BOOKMARK [2][-]{subsection.10.4.1}{Top-quark contribution }{section.10.4}% 61
\BOOKMARK [2][-]{subsection.10.4.2}{Higgs-boson contribution}{section.10.4}% 62
\BOOKMARK [1][-]{section.10.5}{Corrections to the Z boson partial decay widths*}{chapter.10}% 63
\BOOKMARK [1][-]{section.10.6}{Corrections in the gaugeless limit* }{chapter.10}% 64
\BOOKMARK [2][-]{subsection.10.6.1}{Corrections to the \040- parameter}{section.10.6}% 65
\BOOKMARK [2][-]{subsection.10.6.2}{Correction to the Z 1.5mu-1.5mub-1.5mu1.5mub decay rate}{section.10.6}% 66
\BOOKMARK [1][-]{section.10.7}{The S, T and U parameters*}{chapter.10}% 67
\BOOKMARK [0][-]{chapter.11}{Flavor Changing Neutral Currents}{}% 68
\BOOKMARK [1][-]{section.11.1}{Meson-antimeson oscillation}{chapter.11}% 69
\BOOKMARK [1][-]{section.11.2}{FCNC in the SM and the GIM mechanism}{chapter.11}% 70
\BOOKMARK [0][-]{chapter.12}{Anomalies}{}% 71
\BOOKMARK [1][-]{section.12.1}{Axial-vector current anomaly and 0 }{chapter.12}% 72
\BOOKMARK [1][-]{section.12.2}{ABJ anomalies}{chapter.12}% 73
\BOOKMARK [1][-]{section.12.3}{Anomalies in the Standard Model }{chapter.12}% 74
\BOOKMARK [0][-]{chapter.13}{Higgs physics}{}% 75
\BOOKMARK [1][-]{section.13.1}{Theoretical bounds on Mh: triviality and vacuum stability bounds}{chapter.13}% 76
\BOOKMARK [1][-]{section.13.2}{Experimental bounds on Mh}{chapter.13}% 77
\BOOKMARK [1][-]{section.13.3}{Higgs production: gluon-gluon fusion and the parton model}{chapter.13}% 78
\BOOKMARK [1][-]{section.13.4}{Higgs decay modes}{chapter.13}% 79
\BOOKMARK [2][-]{subsection.13.4.1}{h1.5mu-1.5muf-1.5mu1.5muf}{section.13.4}% 80
\BOOKMARK [2][-]{subsection.13.4.2}{hgg and h}{section.13.4}% 81
\BOOKMARK [0][-]{chapter.14}{Exercises}{}% 82
\BOOKMARK [1][-]{section.14.1}{Exercise 1}{chapter.14}% 83
\BOOKMARK [1][-]{section.14.2}{Exercise 2}{chapter.14}% 84
\BOOKMARK [1][-]{section.14.3}{Exercise 3}{chapter.14}% 85
\BOOKMARK [1][-]{section.14.4}{Exercise 4}{chapter.14}% 86
\BOOKMARK [1][-]{section.14.5}{Exercise 5}{chapter.14}% 87
\BOOKMARK [0][-]{appendix.A}{Reference formulae}{}% 88
\BOOKMARK [1][-]{section.A.1}{ matrices and Fierz identities}{appendix.A}% 89
\BOOKMARK [1][-]{section.A.2}{Cross sections and decay rates}{appendix.A}% 90
