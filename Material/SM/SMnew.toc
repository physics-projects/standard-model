\select@language {english}
\contentsline {chapter}{\numberline {1}Physics of flavor}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}The SM Lagrangian}{9}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Bosons}{10}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Fermions and flavor}{12}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}The SM Lagrangian}{16}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Discrete symmetries }{17}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Sakharov conditions}{20}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}Neutrino masses}{22}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Dirac and Majorana mass terms}{22}{subsection.1.3.1}
\contentsline {subsubsection}{The see-saw mechanism}{23}{subsubsection*.7}
\contentsline {subsection}{\numberline {1.3.2}The SM flavor group}{25}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Processes in flavor physics}{26}{subsection.1.3.3}
\contentsline {subsubsection}{Muon decay}{26}{subsubsection*.8}
\contentsline {subsubsection}{Lepton flavor violation}{27}{subsubsection*.9}
\contentsline {subsubsection}{Neutrinoless double beta decay}{28}{subsubsection*.10}
\contentsline {section}{\numberline {1.4}Chiral symmetries of QCD }{29}{section.1.4}
\contentsline {section}{\numberline {1.5}The $V_{\mathrm {CKM}}$ matrix}{33}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}$V_{\mathrm {CKM}}$ matrix elements}{33}{subsection.1.5.1}
\contentsline {subsubsection}{$|V_{ud}|$ -- superallowed $\beta -$decays}{33}{subsubsection*.14}
\contentsline {subsubsection}{$|V_{us}|$ -- semileptonic kaon decay}{35}{subsubsection*.15}
\contentsline {subsubsection}{$|V_{cb}|$ and $|V_{ub}|$ -- $B$ mesons semileptonic decays}{36}{subsubsection*.16}
\contentsline {subsubsection}{Summary and $|V_{ti}|$ elements}{36}{subsubsection*.17}
\contentsline {subsection}{\numberline {1.5.2}Unitarity triangle and $CP$ violation}{37}{subsection.1.5.2}
\contentsline {section}{\numberline {1.6}Lepton flavor universality}{42}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Pion decay}{42}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Tau decays}{45}{subsection.1.6.2}
\contentsline {section}{\numberline {1.7}Neutrino oscillation}{47}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}Oscillation in vacuum}{47}{subsection.1.7.1}
\contentsline {subsection}{\numberline {1.7.2}Oscillation in matter and the MSW effect}{52}{subsection.1.7.2}
\contentsline {chapter}{\numberline {2}EFTs and phenomenology at tree level}{61}{chapter.2}
\contentsline {section}{\numberline {2.1}Construction of EFTs}{61}{section.2.1}
\contentsline {subsubsection}{$\psi -\phi $ Yukawa toy model}{61}{subsubsection*.23}
\contentsline {subsubsection}{Fermi theory and the values of $G_F$ and $v$}{63}{subsubsection*.24}
\contentsline {subsubsection}{See-saw mechanism and the dimension 5 Weinberg operator}{65}{subsubsection*.25}
\contentsline {section}{\numberline {2.2}NC processes and the values of $\lowercase {g}_V$ and $\lowercase {g}_A$ }{65}{section.2.2}
\contentsline {section}{\numberline {2.3}The \lowercase {$e^+ e^- \rightarrow \mu ^+\mu ^-$} process in the SM}{69}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Low energy limit $G_Fs\ll 1$}{70}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}LEP energy limit: $\sqrt {s}\sim M_Z$}{73}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Partial wave unitarity bounds}{78}{section.2.4}
\contentsline {section}{\numberline {2.5}The test of the non abelian structure of the SM}{80}{section.2.5}
\contentsline {subsubsection}{Amplitude for $e_R^-$}{83}{subsubsection*.29}
\contentsline {section}{\numberline {2.6}The Goldstone boson equivalence theorem}{85}{section.2.6}
\contentsline {chapter}{\numberline {3}Phenomenology at loop level}{93}{chapter.3}
\contentsline {section}{\numberline {3.1}Renormalization and dimensional regularization}{94}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Renormalization program: two steps}{95}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Dimensional regularization and subtraction scheme}{95}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Vacuum polarization}{100}{section.3.2}
\contentsline {section}{\numberline {3.3}Electron self-energy}{105}{section.3.3}
\contentsline {section}{\numberline {3.4}Vertex correction}{106}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}The anomalous magnetic moment $g-2$}{108}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}The vertex renormalization and Ward identity of QED}{111}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}ElectroWeak Precision Tests}{115}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Oblique corrections}{117}{subsection.3.5.1}
\contentsline {subsubsection}{Computation of EW vacuum polarization loops}{122}{subsubsection*.33}
\contentsline {subsection}{\numberline {3.5.2}Custodial $SU(2)_C$ symmetry}{125}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Flavor Changing Neutral Currents}{145}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Meson-antimeson oscillation}{145}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}FCNC in the SM and the GIM mechanism}{149}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}The hierarchy problem}{151}{subsection.3.6.3}
\contentsline {section}{\numberline {3.7}Anomalies}{152}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Axial-vector current Ward identity anomaly and $\pi ^0 \rightarrow \gamma \gamma $}{153}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}ABJ anomalies in non-abelian theories }{159}{subsection.3.7.2}
\contentsline {section}{\numberline {3.8}Higgs physics}{161}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Theoretical bounds on $M_h$: triviality and vacuum stability bounds}{161}{subsection.3.8.1}
\contentsline {subsection}{\numberline {3.8.2}Experimental bounds on $M_h$}{170}{subsection.3.8.2}
\contentsline {subsection}{\numberline {3.8.3}Higgs production: gluon-gluon fusion and the parton model}{173}{subsection.3.8.3}
\contentsline {subsection}{\numberline {3.8.4}Higgs decay modes}{175}{subsection.3.8.4}
\contentsline {subsubsection}{Full calculation of the diagrams*}{178}{subsubsection*.45}
\contentsline {chapter}{\numberline {4}Exercises}{189}{chapter.4}
\contentsline {section}{\numberline {4.1}Exercise 1}{189}{section.4.1}
\contentsline {subparagraph}{Text}{189}{subparagraph*.46}
\contentsline {subparagraph}{Solution}{189}{subparagraph*.47}
\contentsline {section}{\numberline {4.2}Exercise 2}{193}{section.4.2}
\contentsline {section}{\numberline {4.3}Exercise 3}{202}{section.4.3}
\contentsline {section}{\numberline {4.4}Exercise 4}{206}{section.4.4}
\contentsline {section}{\numberline {4.5}Exercise 5}{209}{section.4.5}
\contentsline {subparagraph}{Text}{209}{subparagraph*.54}
\contentsline {subparagraph}{Solution}{210}{subparagraph*.55}
\contentsline {chapter}{\numberline {A}Reference formulae}{217}{appendix.A}
\contentsline {section}{\numberline {A.1}$\lowercase {\gamma }$ matrices and Fierz identities}{217}{section.A.1}
\contentsline {section}{\numberline {A.2}Cross sections and decay rates}{218}{section.A.2}
